var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var isProd = process.env.NODE_ENV === 'production';

var babelplugins = [
    "babel-plugin-transform-private-properties",
    "transform-decorators-legacy",
    "transform-class-properties",
    "transform-object-rest-spread",
    "transform-strict-mode"
];

var minifyplugins = [
    'minify-constant-folding'
    ,'minify-flip-comparisons'
    ,'minify-guarded-expressions'
    ,'minify-simplify'
    ,'transform-merge-sibling-variables'
    ,'transform-remove-console'
    ,'transform-remove-debugger'
];

var babelfinalplugins = isProd ? babelplugins.concat(minifyplugins) : babelplugins;

var plugins = [
    new CleanWebpackPlugin(['bin'], {
        verbose: true
    }),

    new HtmlWebpackPlugin({
        title: '',
        filename: '../index.html',
        template: './src/index.html',
        minify: {
            collapseWhitespace: true
        }
    }),

    new ExtractTextPlugin('../css/style.css')
];

if(!isProd){
    plugins.push(
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: {
                baseDir: ['./bin']
            }
        })
    );
}

module.exports = {
    devtool: 'source-map',
    entry: './src/js/Main',
    output: {
        path: path.resolve(__dirname, 'bin/js'),
        filename: 'script.js',
        publicPath: '/'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                include: [path.resolve(__dirname, 'src/js/')],
                loader: 'babel-loader',
                options: {
                    presets: [
                        'react',
                        [
                            'env', {
                                "targets": {
                                    "browsers": ["IE 9"]
                                }
                            }
                        ]
                    ],
                    plugins: babelfinalplugins,
                    env: {
                        production: {
                            presets: ['minify']
                        }
                    }
                }
            },

            {
                test: /\.scss$/,
                include: [
                    path.resolve(__dirname, 'src/sass/')
                ],
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                includePaths: [path.resolve(__dirname, 'node_modules/compass-mixins/lib')]
                            }
                        }
                    ]
                })
            },

            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                include: [path.resolve(__dirname, 'src/images')],
                loaders: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '/[name].[ext]',
                            outputPath: '../images'
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg: {
                                quality: 65
                            },
                            pngquant: {
                                quality: "65-90",
                                speed: 4
                            }
                        }
                    }
                ]
            }
        ]
    },



    plugins: plugins,

    watch: !isProd
}
