import '../sass/style.scss';

import React from 'react';
import ReactDom from 'react-dom';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';

import Home from './components/screens/Home';

import Store from './redux/Store';

window.onload = () => {
    var root = document.createElement('div');

    ReactDom.render(
        <Provider store={Store.store}>
            <ConnectedRouter history={Store.history}>
                <Route path='/' component={Home} />
            </ConnectedRouter>
        </Provider>,
        root
    );
}
