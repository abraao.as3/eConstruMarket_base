import { routerMiddleware } from 'react-router-redux';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import createHystory from 'history/createBrowserHistory';

import reducers from './reducers';

const history = createHystory();
const historyMiddleware = routerMiddleware(history);

const store = createStore(
    reducers,
    applyMiddleware(logger, historyMiddleware, thunk, promise())
);

export default {
    history,
    store
}
