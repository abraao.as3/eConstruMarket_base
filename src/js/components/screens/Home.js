import autobind from 'autobind-decorator';
import React from 'react';

@autobind
class Home extends React.Component{
    render(){
        return (
            <div>Home</div>
        );
    }
}

export default Home;
